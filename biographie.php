<!DOCTYPE html>
<html lang="fr">

<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Jocelyne Gotschaux, artiste peintre, peinture, lyon, communay" />
  	<meta name="robots" content="index, follow" />
  	<meta name="author" content="Yannis Solémalé, Loick Bouchaut" />
  	<meta name="description" content="Jocelyne Gotschaux - artiste peintre, Lyon | Communay, 06 83 14 03 23" />

    <title>Biographie</title>

    <?php include_once('include/head.php');?>

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

	<?php include_once('include/header.php'); ?>

    <!-- Intro Header -->
	
    <!-- About Section -->
    <div id="main">
		<section id="about" class="content-section text-center">
			<div class="about-section-test">
			</div>

			<div id="text">
				<div class="container">
				<!-- Première section à propos -->
					<div class="row">
						<div class="col-sm-6 col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1">
							<h2>A mon propos</h2>

							<p>Depuis près d’une vingtaine d’années, la peinture est mon passe- temps  favori.</p>
							<br>
   							<p>Je décide donc de créer dans mon village une association de peinture, et à ce jour quatre cours sont animés par des peintres professionnels.</p>
       						<br>
       						<p>Si je commence par une peinture figurative, peu à peu je travaille les drapés et profite de mes connaissances dans ce style, ainsi que l’interprétation tirée de mes voyages pour peindre déserts, voiles, touaregs.</p>  
       						<br>
       						<p>L’un de ces touaregs deviendra le logo de l’association «  couleurs d’ailleurs ».</p>
						</div>

						<div class="col-sm-6 col-md-5 col-lg-5">
							<img alt="Jocelyne Gotschaux 1" width="80%" src="img/photo/Jocelyne_Gotschaux_1.jpg" style="margin-bottom: 50px;">
						</div>
					 </div>

					 <!-- Seconde section à propos -->
					<div class="row">
						<div class="col-sm-6 col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1">
							<img alt="Jocelyne Gotschaux 2" src="img/photo/Jocelyne_Gotschaux_2.jpg"  width="80%" style="margin-bottom: 50px;">
						</div>

						<div class="col-sm-6 col-md-5 col-lg-5">
 							<P>Peu à peu ma peinture se libère d’un certain académisme. Je donne libre cours à mes émotions.</P> 
 							<br>
 							<p>Celles-ci sont toujours le point de départ de mes tableaux, dont les thèmes sont de ce fait variés.</P>
 							<br>
 							<p>Mon inspiration vient des voyages, de la découverte des grands espaces.</p>
 							<br>
 							<hr>
 							<h2>Mes distinctions</h2>
 							<p style="text-align: left;">
 							<br>2000    premier prix du salon de Saint Fons
				            <br>2006    premier prix du salon d 'Ambronay
				            <br>2010    Prix de la municipalité, médaille de la ville d' Ambronay
				            <br>2011    Prix coup de cœur de l'exposition " les sansoucistes "Lyon 8°
				            <br>2015    Médaille d'argent du salon de Rives de Gier
				            </p>
						</div>
					</div>

				</div>
			</div>
		</section>
	</div>

	

    <!-- Technique Section -->
    <div id="main">
		<section id="download" class="content-section text-center">
			<div class="download-section-test">
			</div>

			<div id="text">
				<div class="container">

					<div class="row">
						<div class="col-sm-6 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
							<hr>
							<h2>Ma tecnhique</h2>
							<p>Puis j’évolue, du pinceau, je passe rapidement au couteau, je peins à présent avec des vibrations colorées en relief.</p>	
						</div>
						
					</div>

						<div class="row">
							<div class="col-sm-12 col-md-12 col-lg-12">
							<br><br><br><br>
							<p>Pour moi, c’est un véritable moment de grâce, quand en peignant, je m’évade dans le tableau, et petit à petit ce n’est plus moi qui guide le pinceau, le couteau, mais c’est la peinture qui me guide.</p>
							<br>
							<p>Je suis dans un état au-delà de l’explication.</p>
							<p>J’espère que, comme moi, une passion vous anime car elle permet de grandir, de nous porter dans chacun des moments de la vie </p>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</section>
	</div>

	<hr>
	
    <!-- Contact Section -->
    <section id="contact" class="content-section text-center">
    	<div class="contact-section">
	        <div class="row">
	            <div class="col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
	                
					<h2>Contact</h2>
						<p>N'hésitez pas à me contacter pour tout renseignement ou prise de rendez vous :</p>
						<p>06 83 14 03 23</p>

						<p><a href="mailto:asscouleurs@yahoo.fr">asscouleurs@yahoo.fr</a></p>
						
						<p>Consultations:
						<br>1 Allée du Télégraphe, 69360 Communay
						<p></p>
						<p></p>
				</div>
            </div>
        </div>
    </section>



    <!-- Footer -->
    <footer>
	<?php include_once('include/footer.php');?>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/grayscale.js"></script>

</body>

</html>