<!DOCTYPE html>
<html lang="fr">

<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Jocelyne Gotschaux, artiste peintre, peinture, lyon, communay" />
  	<meta name="robots" content="index, follow" />
  	<meta name="author" content="Yannis Solémalé, Loick Bouchaut" />
  	<meta name="description" content="Jocelyne Gotschaux - artiste peintre, Lyon | Communay, 06 83 14 03 23" />

    <title>Jocelyne Gotschaux - artiste peintre</title>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/grayscale.js"></script>

    <?php include_once('include/head.php');?>

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

	<?php include_once('include/header.php'); ?>
    
    <script type="javascript">
		$('.carousel').carousel({
		  interval: 2000
		})
		</script>

    <!-- Intro Header -->
    <!-- <div class="container"> -->
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12">

				<div id="carousel-example-generic" class="carousel slide cp" data-ride="carousel">
					  <!-- Indicators -->
					  <ol class="carousel-indicators">
						<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
						<li data-target="#carousel-example-generic" data-slide-to="1"></li>
						<li data-target="#carousel-example-generic" data-slide-to="2"></li>
						<li data-target="#carousel-example-generic" data-slide-to="3"></li>
					  </ol>

					  <!-- Wrapper for slides -->
					  <div class="carousel-inner cip" role="listbox">

						<div class="fhp item active">
						  
							<!-- <header class="intro" style="background: url(img/photo/carousel/849486d5d33ed462ff4167606a11968e5ae3ab0aa0ac24156.jpg) no-repeat bottom center scroll; background-size: 60% 100% ; "> -->
							<header class="intro">
								<div class="intro-body">
									<div class="container">
										<div class="row">
					      					<img style="max-height: 600px; max-width: 100%; display: block; margin-left: auto; margin-right: auto" src="img/photo/carousel/849486d5d33ed462ff4167606a11968e5ae3ab0aa0ac24156.jpg">
										</div>
									</div>
								</div>
							</header>
						  
						</div>
						
						<div class="fhp item">
						  
							<!-- <header class="intro" style="background: url(img/photo/carousel/7617801ac44ce5fd541cf02197af26b4eef0bc7f8bcb25767.jpg) no-repeat bottom center scroll; background-size: 100% 100%; "> -->
							<header class="intro">
								<div class="intro-body">
									<div class="container">
										<div class="row">
					      					<img style="max-height: 600px; max-width: 100%; display: block; margin-left: auto; margin-right: auto" src="img/photo/carousel/7617801ac44ce5fd541cf02197af26b4eef0bc7f8bcb25767.jpg">
										</div>
									</div>
								</div>
							</header>
						  
						</div>
						
						
						<div class="fhp item">
						  
							<!-- <header class="intro" style="background: url(img/photo/carousel/31428dd95a7c1c7706f8becd8a999104aca0c6fde691b22754.jpg) no-repeat bottom center scroll; background-size: 40% 100%; "> -->
							<header class="intro">
								<div class="intro-body">
									<div class="container">
										<div class="row">
					      					<img style="max-height: 600px; max-width: 100%; display: block; margin-left: auto; margin-right: auto" src="img/photo/carousel/31428dd95a7c1c7706f8becd8a999104aca0c6fde691b22754.jpg">
										</div>
									</div>
								</div>
							</header>
						  
						</div>
						
						
						<div class="fhp item">
						  
							<!-- <header class="intro" style="background: url(img/photo/carousel/16018aa3618930b2392d0bbfe803c1b264f7a0777eeb75216.jpg) no-repeat bottom center scroll; background-size: 100% 100%; "> -->
							<header class="intro">
								<div class="intro-body">
									<div class="container">
										<div class="row">
					      					<img style="max-height: 600px; max-width: 100%; display: block; margin-left: auto; margin-right: auto" src="img/photo/carousel/16018aa3618930b2392d0bbfe803c1b264f7a0777eeb75216.jpg">
										</div>
									</div>
								</div>
							</header>
						  
						</div>


					  </div>

					</div>
				</div>
			</div>
		<!-- </div> -->

		

		<br>

    <!-- Main Section -->
    <div id="main">
		<section id="main" class="content-section text-center">
			<div class="main-section-test">
			</div>

			<div id="text">
				<div class="container">
				<br>
					<div class="row">
							<p>Bienvenue sur mon site, suivez le guide pour une découverte de l'univers passionnant de la peinture avec Jocelyne Gotschaux.</p>
					</div>
					<br>

						<?php


						include_once('include/connexion.php');
					

						global $bdd;

						$req = $bdd->prepare('SELECT DISTINCT A.id, A.nom FROM peintre_categories A INNER JOIN peintre_tableaux B ON A.id = B.categorieid WHERE A.isvisible = 1');
						$req->execute();

						$data = $req->fetchAll();
						
						$loop = 0;		
						foreach($data as $cat)
						{
						$loop ++;

						$categorieid = $cat['id'];

						$req = $bdd->prepare('SELECT titre, url FROM peintre_tableaux WHERE isvisible = 1 AND categorieid = :categorieid LIMIT 1');
						$req->bindParam(':categorieid', $categorieid);
						$req->execute();

						$data = $req->fetchAll();
							
						?>	
					
					<?php if ($loop == 1) 
					{echo '<div class="row">';}	
					?>

	            	 <div class="col-sm-6 col-md-4 col-lg-4">
					    <div class="thumbnail">
							<a href="galerie.php?id=<?php echo $cat['id'];?>">
					      		<img style="max-height: 250px;" src="<?php echo 'img/photo/categorie_photo/'.substr($data[0]['url'],14); ?>" alt="<?php echo $data[0]['titre']; ?>">
					      	</a>
					      <div class="caption">
					        <h3><a href="galerie.php?id=<?php echo $cat['id'];?>"><?php echo $cat['nom']; ?></a></h3>
					      </div>
					    </div>
					  </div>
					
					<?php if ($loop == 3) 
					{	echo '</div>';
						$loop = 0;	}	
					?>

						<?php
						
						}
						
						?>


				<br>

				</div>
			</div>
		</section>
	</div>

    <!-- Footer -->
    <footer>
	<?php include_once('include/footer.php');?>
    </footer>

</body>

</html>