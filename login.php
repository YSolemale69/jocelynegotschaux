<?php

$erreur = 0;

//print_r($_POST);

if(isset($_POST['user']) && $_POST['user'] !== '' && isset($_POST['password']) && $_POST['password'] !== '')
{
	if($_POST['user'] == "jgotschaux" && $_POST['password'] == "P4s5wOrD")
	{
		session_start();
		$_SESSION["login"] = "ok";
		
		header("location:./administration.php");
	}
	else
	{
		$erreur = 1;
	}
	
	
}

?>


<!DOCTYPE html>
<html lang="fr">

<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Jocelyne Gotschaux, artiste peintre, peinture, lyon, communay" />
  	<meta name="robots" content="index, follow" />
  	<meta name="author" content="Yannis Solémalé, Loick Bouchaut" />
  	<meta name="description" content="Jocelyne Gotschaux - artiste peintre, Lyon | Communay, 06 83 14 03 23" />

    <title>Login</title>

    <?php include_once('include/head.php');?>

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

	<?php include_once('include/header.php'); 
	
		if($erreur)
		{
			echo "identifiant ou mot de passe incorrect.";
		}
	
	?>
	
	
	
	
	<form method="post" name="admin" action="" class="form-horizontal">
	  <div class="form-group">
		<label for="user" class="col-sm-2 control-label">nom de compte</label>
		<div class="col-sm-2">
		  <input type="text" class="form-control" name="user" placeholder="nom de compte" required>
		</div>
	  </div>
	  
	  <div class="form-group">
		<label for="password" class="col-sm-2 control-label">Mot de passe</label>
		<div class="col-sm-2">
		  <input type="password" class="form-control" name="password" placeholder="password" required>
		</div>
	  </div>

	  <div class="form-group">
		<div class="col-sm-offset-2 col-sm-2">
		  <button type="submit" class="btn btn-default">Se connecter</button>
		</div>
	  </div>
	</form>
	

	
    <!-- Footer -->
    <footer>
	<?php include_once('include/footer.php');?>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/grayscale.js"></script>

</body>

</html>

