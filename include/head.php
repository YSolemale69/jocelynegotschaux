    <link rel="icon" href="img/favicon.ico" />

    <!-- Core JS file -->
    <script src="js/photoswipe.min.js"></script> 

    <!-- UI JS file -->
    <script src="js/photoswipe-ui-default.min.js"></script> 

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/business-casual.css" rel="stylesheet">