Informations légales

L'intégralité de ce site est la proprièté exclusive de Jocelyne Gotschaux. Il est protégé par les droits de propriètés intellectuelles.

Toute représentation ou altération, totale ou partielle est interdite.

Toute copie de texte, d'images, ou photographies est strictement interdite.

La création de liens hypertextes vers ce site internet   nécessite une autorisation préalable et écrite par Jocelyne Gotschaux.

Cette dernière ne peut en aucune circonstance être tenue responsable du contenu des sites liés au sien.

Hébergeur du site
OVH SARL

7, place de la Gare

BP 70109

57201 Sarreguemines Cedex

CNIL
Ce site a fait l'objet d'un enregistrement auprès de la CNIL le JJ/MM/AAAA sous le numéro : [à compléter]