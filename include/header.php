	<div class="brand">Jocelyne Gotschaux</div>
    <div class="address-bar">Artiste peintre | Lyon | Communay</div>
    <hr class="head">

    <!-- Navigation -->
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- navbar-brand is hidden on larger screens, but visible when the menu is collapsed -->
                <a class="navbar-brand" href="index.php">Jocelyne Gotschaux</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>

                    <li>
                        <a class="page-scroll" href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
                    </li>

                    <li>
                        <a class="page-scroll" href="biographie.php">BIOGRAPHIE</a>
                    </li>

                    <li>
                        <a class="page-scroll" href="association.php">L'ASSOCIATION</a>
                    </li>

                    <li>
                        <a class="page-scroll" href="galerie.php">GALERIE</a>
                    </li>

                    <li>
                        <a class="page-scroll" href="expositions.php">MES EXPOSITIONS</a>
                    </li>

                    <li>
                        <a class="page-scroll" href="contact.php">CONTACT</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <hr class="head">