<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Jocelyne Gotschaux, artiste peintre, peinture, lyon, communay" />
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Yannis Solémalé, Loick Bouchaut" />
    <meta name="description" content="Jocelyne Gotschaux - artiste peintre, Lyon | Communay, 06 83 14 03 23" />

    <title>404</title>
    
    <?php include_once('include/head.php');?>


    <link rel="icon" href="img/favicon.ico" />

</head>

<body>
    <div class="row">
        <br><br><br>
            <div class="col-sm-5 col-sm-offset-4 col-md-5 col-md-offset-4 col-lg-5 col-lg-offset-4">
                <h1>Oups, Erreur 404 ...</h1>
            </div>
    </div>
</body>

</html>