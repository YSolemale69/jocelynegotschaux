<!DOCTYPE html>
<html lang="fr">

<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Jocelyne Gotschaux, artiste peintre, peinture, lyon, communay" />
  	<meta name="robots" content="index, follow" />
  	<meta name="author" content="Yannis Solémalé, Loick Bouchaut" />
  	<meta name="description" content="Jocelyne Gotschaux - artiste peintre, Lyon | Communay, 06 83 14 03 23" />

    <title>Association</title>

    <?php include_once('include/head.php');?>

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

	<?php include_once('include/header.php'); ?>
 

    
    <div id="main">
		<section id="presentation" class="content-section text-center">
			<div class="presentation-section-test">
			</div>

			<div id="text">
				<div class="container">

					<!-- Section Presentation et Cours-->
					<div class="row">
						<div class="col-sm-6 col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1">

							<h2>Association « couleurs d’ailleurs »</h2>

							<p>En 2004, je mets en place l’association « couleurs d’ailleurs » dans mon village de communay.
							 Trois types d’activité :  les cours , les expositions, les voyages.
							</p>

							 <br>
							
							<h3>Les cours</h3> 
							<p>
							<br><strong>Nina David</strong> assurera les premiers cours en 2004, en peinture à l’huile. 
							<br><br><strong>Alain Gonnet</strong> prendra la relève en 2005. 
							<br><br>Devant le nombre de participants un deuxième animateur, Colette Plasson rejoint l'association en 2008 : <strong>Colette Plasson</strong>.
							<br><br><strong>Francis Cordina</strong> animera le 3° cours, l’aquarelle.
							</p>

							<br>

							<p>Puis un quatrième cours de dessin, carnets de voyage avec <strong>Marie Stricher</strong>, 
							<br><br>remplacée en 2014 par <strong>Olivier Brunot</strong>, qui anime des cours de technique mixte et dessin.
							Les quatre cours comptent actuellement une trentaine de participants.
							</p>

							<br><hr><br>

						</div>
						<!-- Image affiches -->
						<div class="col-sm-6 col-md-5 col-lg-5">
							<img alt="" width="80%" src="./img/photo/147957652e855af424851bc535b5abc68973a45ba961926817.jpg" style="margin-bottom: 50px;">
						</div>
					 </div>


					 <!-- Section Expositions -->
					<div class="row">
						<div class="col-sm-6 col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1">

							<h3>Les expositions</h3>

							<p style="text-align: left;">
							<br>La première exposition a été préparée dans le cadre d’un diplôme de fin d’études de ma fille Caroline.
							<br><br>Le logo touareg de l’association apparait à cette occasion.
							<br><br>Nous avons eu la chance pendant plusieurs années d’exposer avec des grands peintres.
							<br><br>2016- 2017 verra le départ de 2 animateurs.
							<br><br>Alain Gonnet nous quitte après 10 ans de bons conseils. Il est remplacé par Jean Jacques Wambst.
							</p>

							<br>

							<p><strong>Francis Cordina</strong> quitte également l’association et sera remplacé par <strong>Anne Baudrand</strong>.
								Notre association de peinture participe à des manifestations de la commune, telles que le week-end « arts et Jardins » au mois de juin et exposition à la médiathèque pour la fête de l’automne.</p>

							<br><hr><br>

						</div>
						<!-- Image logo touareg -->
						<div class="col-sm-6 col-md-5 col-lg-5">
							<p><img alt="diagramme" src="./img/photo/69797bf2bc26f81292e140d2585fbe9bcae58d86b0b14857.jpg" width="70%"></p>
						</div>
					</div>


					<!-- Section Invités -->
					<div class="row">
						<div class="col-sm-6 col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1">

							<h3>Invités d'Honneur</h3>

							<table class=table> 
								<thead> 
									<tr> 
										<th></th> 
										<th>Invités d'Honneur Peintres</th> 
										<th>&nbsp &nbsp &nbsp Invités sculpteurs</th>  
									</tr> 
								</thead> 

								<tbody>  
									<tr><th scope=row>2004</th> <td>Nina DAVID</td> <td></td> </tr>
									<tr><th scope=row>2005</th> <td>Alain GONNET</td> <td></td> </tr>
									<tr><th scope=row>2006</th> <td>Jacques GALLAND</td> <td>Bruno PILLOIX</td> </tr> 
									<tr><th scope=row>2007</th> <td>Colette PLASSON</td> <td>Isabelle ALLAIN</td> </tr> 
									<tr><th scope=row>2008</th> <td>Aya FAYARD</td> <td>Alexandre BERLIOZ et Frédéric BOIRON</td> </tr> 
									<tr><th scope=row>2009</th> <td>Michèle VAN COTTHEM	</td> <td>Jean Marc BELLETON</td> </tr> 
									<tr><th scope=row>2010</th> <td>Jean Noël DELETTRE</td> <td>Jean Marc BUORO et Maxime CADOUX</td> </tr>
									<tr><th scope=row>2017</th> <td>Olivier BRUNOT</td> <td></td> </tr>  
								</tbody> 
							</table>

						</div>

						<div class="col-sm-6 col-md-5  col-lg-5 ">
							
							<h3>Voyages</h3>

							<p>Depuis plusieurs années, j’organise des voyages de fin d’année.</p>
							<p style="text-align: left;">
							<br>2006 :  Morestel
							<br>2007 : les jardins secrets à Rumilly
							<br>2008 : Musée Calvet à Avignon
							<br>2009 : journée médiévale à Cluny
							<br>2010 : musée de Grenoble  plus Chagall
							<br>2011 : Avec les arts ripagériens Martigny, Evian, Chamonix
							<br>2012 : Baux de Provence et sa carrière d’images
							<br>2013 : Dijon
							<br>2014 : Marseille, Le Mucem et notre dame de la garde
							<br>2015 : Saint Antoine l’abbaye, Musée de valence
							<br>2016 : Annecy  Visite de la vieille ville, croisière sur le lac , visite du musée de la cloche.
							<br>2017 : Aix les bains avec la visite théâtralisée du musée Faure ,visite du casino, des anciennes thermes et des palaces.
							</p>

						</div>
					</div>

				</div>
			</div>
		</section>
	</div>

	
	<br><br><br>


    <!-- Footer -->
    <footer>
	<?php include_once('include/footer.php');?>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/grayscale.js"></script>

</body>

</html>