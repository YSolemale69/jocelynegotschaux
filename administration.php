<?php
session_start();

$ok = 0;
$erreur = 0;

if(!(isset($_SESSION["login"])))
{	
	header("location:.");
}



//require_once 'include/define.php';


require_once 'include/connexion.php';


//#########################################################################################################################
//###########												INSERT				 	 							  #########
//#########################################################################################################################
//
//AJOUT TABLEAU
//
if(isset($_POST['titre_tableau']) && $_POST['titre_tableau'] !== '' )
{
		$new_file = "error";
		$pid = "";
		
			//récupération du nom du fichier
			$nomimg = $_FILES["fichier"]["name"];

			$extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png' );
			//1. strrchr renvoie l'extension avec le point (? . ?).
			//2. substr(chaine,1) ignore le premier caract?re de chaine.
			//3. strtolower met l'extension en minuscules.
			$extension_upload = strtolower(  substr(  strrchr($nomimg, '.')  ,1)  );

			$photoname = strstr($nomimg, '.', true);
			

			if ( in_array($extension_upload,$extensions_valides) )
			{
				$var_nom = rand().sha1(rand().$nomimg).rand();
				$nomimg = $var_nom .".". $extension_upload; //genere un nom aleatoire 
			
				//$dir_user = "/user-" . $_SESSION['id']."-".$_SESSION['username'];
				//$dir = ROOT. "/img/tableaux"; //. $dir_user;
				//$dir = "img/tableaux/galerie/"; //. $dir_user;
				//$dir_full_size = "img/tableaux/"; //. $dir_user;

				$dir = "img/tableaux/galerie/"; //. $dir_user;
				$dir_full_size = "img/tableaux/"; //. $dir_user;

				
				if (!file_exists($dir)) 
				{
					mkdir($dir, 0777, true);
				}
				
				$new_file_resized = $dir . "/{$nomimg}";
				$new_file_full_size = $dir_full_size . "/{$nomimg}";
				$resultat_full_size = copy($_FILES["fichier"]['tmp_name'], $new_file_full_size);

				//récupéartion de l'image
				$fn = $_FILES['fichier']['tmp_name'];
				//taille de l'image
				$size = getimagesize($fn);

				//calcul du ratio
				$ratio = $size[0]/$size[1]; // width/height
				if( $ratio > 1) {
				    $width = 594;
				    $height = 594/$ratio;
				}
				else {
				    $width = 594*$ratio;
				    $height = 594;
				}

				$src = imagecreatefromstring(file_get_contents($fn));
				$dst = imagecreatetruecolor($width,$height);

				//formation de l'image
				imagecopyresampled($dst,$src,0,0,0,0,$width,$height,$size[0],$size[1]);
				imagedestroy($src);
				$resultat = imagepng($dst,$new_file_resized); // adjust format as needed
				imagedestroy($dst);

				//$resultat = move_uploaded_file($_FILES["fichier"]['tmp_name'], $new_file_resized);
				
				

				if( $resultat ) 
				{
				  echo "Envoi réussi";         
				} 
				else 
				{
				  echo "Fichier non envoyé à cause de l'erreur #".$_FILES["fichier"]["error"];
				}
			}

		$titre_tableau = $_POST['titre_tableau'];
		$categorieid = $_POST['categorie'];
		$url = "./img/tableaux/" . $nomimg; 
		$isvisible = 1;
		
		
		try
		{
			$req = $bdd->prepare 
			("
				INSERT INTO peintre_tableaux (titre, categorieid, url, isvisible) 
				VALUES(:titre, :categorieid, :url, :isvisible);
			");

			$req->bindParam(':titre', $titre_tableau);
			$req->bindParam(':categorieid', $categorieid);
			$req->bindParam(':url', $url);
			$req->bindParam(':isvisible', $isvisible);

			$req->execute();	
		}

		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
			// En cas d'erreur, on affiche un message et on arrête tout
		}	
}


//
//AJOUT PHOTO EXPO
//
if(isset($_POST['titre_photo_expo']) && $_POST['titre_photo_expo'] !== '' )
{
		$new_file = "error";
		$pid = "";
		

			$nomimg = $_FILES["fichier"]["name"];
			
			$extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png' );
			//1. strrchr renvoie l'extension avec le point (? . ?).
			//2. substr(chaine,1) ignore le premier caract?re de chaine.
			//3. strtolower met l'extension en minuscules.
			$extension_upload = strtolower(  substr(  strrchr($nomimg, '.')  ,1)  );

			$photoname = strstr($nomimg, '.', true);
			

			if ( in_array($extension_upload,$extensions_valides) )
			{
				$var_nom = rand().sha1(rand().$nomimg).rand();
				$nomimg = $var_nom .".". $extension_upload; //genere un nom aleatoire 
			
				//$dir_user = "/user-" . $_SESSION['id']."-".$_SESSION['username'];
				//$dir = ROOT. "/img/photo/"; //. $dir_user;
				$dir = "img/photo/"; //. $dir_user;

				if (!file_exists($dir)) 
				{
					mkdir($dir, 0777, true);
				}
				
				$new_file = $dir . "/{$nomimg}";
				$resultat = move_uploaded_file($_FILES["fichier"]['tmp_name'], $new_file);
				

				if( $resultat ) 
				{
				  echo "Envoi réussi";         
				} 
				else 
				{
				  echo "Fichier non envoyé à cause de l'erreur #".$_FILES["fichier"]["error"];
				}
			}


		$titre_photo_expo = $_POST['titre_photo_expo'];
		$expositionid = $_POST['exposition'];
		$url = "./img/photo/" . $nomimg; 
		$isvisible = 1;
		
		
		try
		{

			
			$req = $bdd->prepare 
			("
				INSERT INTO peintre_photo (titre, expositionid, url, isvisible) 
				VALUES(:titre, :expositionid, :url, :isvisible);
			");

			$req->bindParam(':titre', $titre_photo_expo);
			$req->bindParam(':expositionid', $expositionid);
			$req->bindParam(':url', $url);
			$req->bindParam(':isvisible', $isvisible);

			$req->execute();	
		}

		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
			// En cas d'erreur, on affiche un message et on arrête tout
		}	
}



//
//AJOUT CATEGORIE
//
if(isset($_POST['nom_categorie']) && $_POST['nom_categorie'] !== '' )
{
		$nom_categorie = $_POST['nom_categorie'];
		$isvisible = 1;
		
		
		try
		{

			
			$req = $bdd->prepare 
			("
				INSERT INTO peintre_categories (nom, isvisible) 
				VALUES(:nom, :isvisible);
			");

			$req->bindParam(':nom', $nom_categorie);
			$req->bindParam(':isvisible', $isvisible);

			$req->execute();	
		}

		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
			// En cas d'erreur, on affiche un message et on arrête tout
		}	
}



//
//AJOUT EXPOSITION
//
if(isset($_POST['nom_exposition']) && $_POST['nom_exposition'] !== '' )
{
		$nom_exposition = $_POST['nom_exposition'];
		$isvisible = 1;
		
		
		try
		{

			
			$req = $bdd->prepare 
			("
				INSERT INTO peintre_expositions (nom, isvisible) 
				VALUES(:nom, :isvisible);
			");

			$req->bindParam(':nom', $nom_exposition);
			$req->bindParam(':isvisible', $isvisible);

			$req->execute();	
		}

		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
			// En cas d'erreur, on affiche un message et on arrête tout
		}	
}

//#########################################################################################################################
//###########								UPDATES 															  #########
//#########################################################################################################################
//
//UPDATE catégories
//
if(isset($_POST['nom_categorie_update']) && $_POST['nom_categorie_update'] !== '' )
{
		$nom_categorie = $_POST['nom_categorie_update'];
		
		if(isset($_POST['isvisible_categorie_update']) && $_POST['isvisible_categorie_update'] == '1') 
			{$isvisible = 1;}
		else
			{$isvisible = 0;}

		$id_categorie = $_POST['id_categorie_update'];
		try
		{

			
			$req = $bdd->prepare 
			("
				UPDATE peintre_categories 
				SET nom = :nom, isvisible = :isvisible
				WHERE id = :id_categorie;
			");

			$req->bindParam(':nom', $nom_categorie);
			$req->bindParam(':isvisible', $isvisible);
			$req->bindParam(':id_categorie', $id_categorie);

			$req->execute();
		}

		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
			// En cas d'erreur, on affiche un message et on arrête tout
		}	
}



//
//UPDATE expositions
//
if(isset($_POST['nom_exposition_update']) && $_POST['nom_exposition_update'] !== '' )
{
		$nom_exposition = $_POST['nom_exposition_update'];
		
		if(isset($_POST['isvisible_exposition_update']) && $_POST['isvisible_exposition_update'] == '1') 
			{$isvisible = 1;}
		else
			{$isvisible = 0;}

		$id_exposition = $_POST['id_exposition_update'];
		try
		{

			
			$req = $bdd->prepare 
			("
				UPDATE peintre_expositions 
				SET nom = :nom, isvisible = :isvisible
				WHERE id = :id_exposition;
			");

			$req->bindParam(':nom', $nom_exposition);
			$req->bindParam(':isvisible', $isvisible);
			$req->bindParam(':id_exposition', $id_exposition);

			$req->execute();
		}

		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
			// En cas d'erreur, on affiche un message et on arrête tout
		}	
}



//
//UPDATE tableaux catégories
//
if(isset($_POST['nom_tableau_update']) && $_POST['nom_tableau_update'] !== '' )
{
		$nom_tableau = $_POST['nom_tableau_update'];

		if(isset($_POST['isvisible_tableau_update']) && $_POST['isvisible_tableau_update'] == '1') 
			{$isvisible = 1;}
		else
			{$isvisible = 0;}

		$id_tableau = $_POST['id_tableau_update'];
		try
		{

			
			$req = $bdd->prepare 
			("
				UPDATE peintre_tableaux 
				SET titre = :nom, isvisible = :isvisible
				WHERE id = :id_tableau;
			");

			$req->bindParam(':nom', $nom_tableau);
			$req->bindParam(':isvisible', $isvisible);
			$req->bindParam(':id_tableau', $id_tableau);

			$req->execute();
		}

		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
			// En cas d'erreur, on affiche un message et on arrête tout
		}	
}



//
//UPDATE tableaux expositions
//
if(isset($_POST['nom_tableau_expo_update']) && $_POST['nom_tableau_expo_update'] !== '' )
{
		$nom_photo = $_POST['nom_tableau_expo_update'];
		
		if(isset($_POST['isvisible_tableau_expo_update']) && $_POST['isvisible_tableau_expo_update'] == '1') 
			{$isvisible = 1;}
		else
			{$isvisible = 0;}

		$id_tableau = $_POST['id_tableau_expo_update'];
		try
		{

			
			$req = $bdd->prepare 
			("
				UPDATE peintre_photo
				SET titre = :nom, isvisible = :isvisible
				WHERE id = :id_tableau;
			");

			$req->bindParam(':nom', $nom_photo);
			$req->bindParam(':isvisible', $isvisible);
			$req->bindParam(':id_tableau', $id_tableau);

			$req->execute();
		}

		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
			// En cas d'erreur, on affiche un message et on arrête tout
		}	
}
?>


<!DOCTYPE html>
<html lang="fr">

<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Jocelyne Gotschaux, artiste peintre, peinture, lyon, communay" />
  	<meta name="robots" content="index, follow" />
  	<meta name="author" content="Yannis Solémalé, Loick Bouchaut" />
  	<meta name="description" content="Jocelyne Gotschaux - artiste peintre, Lyon | Communay, 06 83 14 03 23" />

    <title>Administration</title>

    <?php include_once('include/head.php');?>

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">


	<?php
		include_once('include/header.php');
	?>

<div class="row">
    <div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
<!-- ######################################################################################################################### -->
	<!-- Ajouter un tableau -->
	<form method="post" enctype="multipart/form-data" name="tableau" action="" class="form-horizontal">
	
	
	<legend>Ajouter un tableau</legend>
	  <div class="form-group">
		<label for="titre_tableau" class="col-sm-2 col-md-2 col-lg-2 control-label">Titre</label>
		<div class="col-sm-5 col-md-5 col-lg-5">
		  <input type="text" class="form-control" name="titre_tableau" placeholder="Titre du tableau" required>
		</div>
	  </div>
	  
	  
	  
	 <div class="form-group">
		<label for="categorie" class="col-sm-2 col-md-2 col-lg-2 control-label">categorie</label>
		<div class="col-sm-10 col-md-10 col-lg-10">
			<select name="categorie" id="categorie">
				<option value="">Sélectionnez une catégorie</option>
			<?php
			
				$req = $bdd->prepare('SELECT * FROM peintre_categories');

				$req->execute();

				$te = $req->fetchAll();
				
				foreach($te as $c)
				{
					echo '<option value="'. $c['id'] .'">'. utf8_encode($c['nom']) .'</option>';
				}

			?>
			</select>
		</div>
	  </div>
	  
	  
	  
	  <div class="form-group">
		<label for="fichier" class="col-sm-2 col-md-2 col-lg-2 control-label">Fichier</label>
		<div class="col-sm-10 col-md-10 col-lg-10">
		  <input id="fichier" type="file" name="fichier">
		</div>
	  </div>
	  


	  <div class="form-group">
		<div class="col-sm-offset-2 col-sm-10 col-md-offset-2 col-md-10 col-lg-offset-2 col-lg-10">
		  <button type="submit" class="btn btn-default">Valider</button>
		</div>
	  </div>
	</form>

	<br>
	<br>




	<!-- ajouter une photo à une exposition -->
	<form method="post" enctype="multipart/form-data" name="photo_expo" action="" class="form-horizontal">
	
	
	<legend>Ajouter une photo à une exposition</legend>
	  <div class="form-group">
		<label for="titre_photo_expo" class="col-sm-2 col-md-2 col-lg-2 control-label">Titre</label>
		<div class="col-sm-5 col-md-5 col-lg-5">
		  <input type="text" class="form-control" name="titre_photo_expo" placeholder="Titre de la photo" required>
		</div>
	  </div>
	  

	  	  
	  <div class="form-group">
		<label for="exposition" class="col-sm-2 col-md-2 col-lg-2 control-label">exposition</label>
		<div class="col-sm-10 col-md-10 col-lg-10">
			<select name="exposition" id="exposition">
				<option value="">Sélectionnez une exposition</option>
			<?php
			
				$req = $bdd->prepare('SELECT * FROM peintre_expositions');

				$req->execute();

				$te = $req->fetchAll();
				
				foreach($te as $e)
				{
					echo '<option value="'. $e['id'] .'">'. utf8_encode($e['nom']) .'</option>';
				}

			?>
			</select>
		</div>
	  </div>
	  
	  
	  
	  <div class="form-group">
		<label for="fichier" class="col-sm-2 col-md-2 col-lg-2 control-label">Fichier</label>
		<div class="col-sm-10 col-md-10 col-lg-10">
		  <input id="fichier" type="file" name="fichier">
		</div>
	  </div>
	  


	  <div class="form-group">
		<div class="col-sm-offset-2 col-sm-10 col-md-offset-2 col-md-10 col-lg-offset-2 col-lg-10">
		  <button type="submit" class="btn btn-default">Valider</button>
		</div>
	  </div>
	</form>


	<br>
	<br>




	<!-- Ajouter une catégorie -->
	<form method="post" enctype="multipart/form-data" name="categorie" action="" class="form-horizontal">
	
	
	<legend>Ajouter une catégorie</legend>
	  <div class="form-group">
		<label for="nom_categorie" class="col-sm-2 col-md-2 col-lg-2 control-label">Nom</label>
		<div class="col-sm-5 col-md-5 col-lg-5">
		  <input type="text" class="form-control" name="nom_categorie" placeholder="Nom de catégorie" required>
		</div>
	  </div>
	  


	  <div class="form-group">
		<div class="col-sm-offset-2 col-sm-10 col-md-offset-2 col-md-10 col-lg-offset-2 col-lg-10">
		  <button type="submit" class="btn btn-default">Valider</button>
		</div>
	  </div>
	</form>



	<!-- Ajouter une exposition -->
	<form method="post" enctype="multipart/form-data" name="exposition" action="" class="form-horizontal">
	
	
	<legend>Ajouter une exposition</legend>
	  <div class="form-group">
		<label for="nom_exposition" class="col-sm-2 col-md-2 col-lg-2 control-label">Nom</label>
		<div class="col-sm-5 col-md-5 col-lg-5">
		  <input type="text" class="form-control" name="nom_exposition" placeholder="Nom de l'exposition" required>
		</div>
	  </div>
	  


	  <div class="form-group">
		<div class="col-sm-offset-2 col-sm-10 col-md-offset-2 col-md-10 col-lg-offset-2 col-lg-10">
		  <button type="submit" class="btn btn-default">Valider</button>
		</div>
	  </div>
	</form>



<!-- ######################################################################################################################### -->


	<!-- Tableaux et catégories -->
	<legend>Visualisez et modifiez une catégorie</legend>
	
	<?php 	
	//SELECT catégories
	$req = $bdd->prepare('SELECT id, nom, isvisible FROM peintre_categories');
	$req->execute();

	$data = $req->fetchAll();

	foreach($data as $cat) { ?>	

		<form method="post" enctype="multipart/form-data" name="categories" action="" class="form-horizontal">
		  <div class="form-group">
			<div class="col-sm-offset-3 col-sm-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
			  <input type="hidden" name="id_categorie_update" value="<?php echo $cat['id'];?>">
			  <input type="checkbox" name="isvisible_categorie_update" value="1" <?php if ($cat['isvisible'] == 1) {echo "checked";} ;?>>
			  <input type="text" class="form-control" name="nom_categorie_update" value="<?php echo $cat['nom']; ?>" required>
			</div>
		  </div>		  
		</form>
	<?php 

	//SELECT tableaux
	$categorieid = $cat['id'];
	$req = $bdd->prepare('SELECT id, categorieid, titre, url, isvisible FROM peintre_tableaux WHERE categorieid = :categorieid');
	$req->bindParam(':categorieid', $categorieid);
	$req->execute();

	$row = $req->fetchAll();

	$count = 1; 

	foreach($row as $tab) { 
	
	//ouverture de la div	
	if ($count%4 == 1)
	{echo "<div class='row' style='min-height: 300px;'>";}
		?>
		<div class='col-sm-3 col-md-3 col-lg-3' style='text-align: center; height: 200px;'>		
			<img style='max-height: 150px; display: block;margin: auto;' src='img/tableaux/galerie<?php echo substr($tab['url'],14) ?>' />
			<form method="post" enctype="multipart/form-data" name="tableaux" action="" class="form-horizontal">
			  <div class="form-group">
				  <input type="checkbox" name="isvisible_tableau_update" value="1" <?php if ($tab['isvisible'] == 1) {echo "checked";} ;?>>
				  <input type="hidden" name="id_tableau_update" value="<?php echo $tab['id'];?>">
				  <input type="text" class="form-control" name="nom_tableau_update" value="<?php echo $tab['titre']; ?>" required>
			  </div>	  
			</form>
		</div> 
	
	<?php 
	//fermeture de la div
	if ($count%4 == 0)
	{echo "</div>";}
	
	$count ++;

	}//fin foreach tableau
	//echo " </div> <br>";
	
	//forçage de la fermeture de la div
	if ($count%4 != 1) 
	echo "</div>";
	echo "<hr>";

	}//fin foreach catégorie 
	?>






	<!-- Tableaux et expositions -->
	<legend>Visualisez et modifiez une expositions</legend>
	
	<?php 	
	//SELECT catégories
	$req = $bdd->prepare('SELECT id, nom, isvisible FROM peintre_expositions');
	$req->execute();

	$data = $req->fetchAll();

	foreach($data as $exp) { ?>	
								
		<form method="post" enctype="multipart/form-data" name="tableau&exposition" action="" class="form-horizontal">
		  <div class="form-group">
			<div class="col-sm-offset-3 col-sm-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
			  <input type="hidden" name="id_exposition_update" value="<?php echo $exp['id'];?>">
			  <input type="checkbox" name="isvisible_exposition_update" value="1" <?php if ($exp['isvisible'] == 1) {echo "checked";} ;?>>
			  <input type="text" class="form-control" name="nom_exposition_update" value="<?php echo $exp['nom']; ?>" required>
			</div>
		  </div>	  
		</form>
	<?php 

	//SELECT tableaux expositions
	$expositionid = $exp['id'];
	$req = $bdd->prepare('SELECT id, expositionid, titre, url, isvisible FROM peintre_photo WHERE expositionid = :expositionid');
	$req->bindParam(':expositionid', $expositionid);
	$req->execute();

	$row = $req->fetchAll();

	$count = 1; 

	foreach($row as $tab_expo) { 
	
	//ouverture de la div	
	if ($count%4 == 1)
	{echo "<div class='row' style='min-height: 300px;'>";}
		?>
		<div class='col-sm-3 col-md-3 col-lg-3' style='text-align: center; height: 200px;'>		
			<img style='max-height: 150px; display: block;margin: auto;' src='img/photo<?php echo substr($tab_expo['url'],11) ?>' />
			<form method="post" enctype="multipart/form-data" name="tableaux" action="" class="form-horizontal">
			  <div class="form-group">
				  <input type="checkbox" name="isvisible_tableau_expo_update" value="1" <?php if ($tab_expo['isvisible'] == 1) {echo "checked";} ;?>>
				  <input type="hidden" name="id_tableau_expo_update" value="<?php echo $tab_expo['id'];?>">
				  <input type="text" class="form-control" name="nom_tableau_expo_update" value="<?php echo $tab_expo['titre']; ?>" required>
			  </div>	  
			</form>
		</div> 
	
	<?php 
	//fermeture de la div
	if ($count%4 == 0)
	{echo "</div>";}
	
	$count ++;

	}//fin foreach tableau
	//echo " </div> <br>";
	
	//forçage de la fermeture de la div
	if ($count%4 != 1) 
	echo "</div>";
	echo "<hr>";

	}//fin foreach catégorie 
	?>

	</div>
</div>





	<!-- Footer -->
    <footer>
	<?php include_once('include/footer.php');?>
    </footer>
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/grayscale.js"></script>

</body>

</html>