<?php

require_once 'include/connexion.php';


//si POST
if(isset($_POST['nom']) && $_POST['nom'] !== '' )
{
		//contact
		if(isset($_POST['contact']) && $_POST['contact'] !== '' )
		{
			
			//Récupération des infos du contenu de mail
			$nom = $_POST['nom'];
			$email = $_POST['email'];
			$tel = $_POST['tel'];
			$message = $_POST['message'];
			
			try
			{
				//Constitution du mail
				$objet = 'Nouveau message de votre site internet';
				$text = "Nom : {$nom} \nEmail : {$email} \nTéléphone : {$tel} \n\n{$message}";
				$headers = 'From: jocelyne.gotschaux@ssdgc.fr' . "\r\n" .
						     'Reply-To: '.$email;

				//Envoi du mail		
				mail('asscouleurs@yahoo.fr', $objet, $text, $headers);

				//Message de validation
				echo "Message envoyé";				
			}

			catch(Exception $e)
			{
				die('Erreur : '.$e->getMessage());
				// En cas d'erreur, on affiche un message et on arrête tout
			}
		}
		
		
		
		//livre d'or
		if(isset($_POST['livreor']) && $_POST['livreor'] !== '' )
		{
			$pseudo = $_POST['nom'];
			$message = $_POST['message'];

			try
			{

				
				$req = $bdd->prepare 
				("
					INSERT INTO peintre_livreor 
						(pseudo, message) 
					VALUES
						(:pseudo, :message);
				");


				$req->bindParam(':pseudo', $pseudo);
				$req->bindParam(':message', $message);

				$req->execute();

				echo("Message enregistré");
			}

			catch(Exception $e)
			{
				die('Erreur : '.$e->getMessage());
				// En cas d'erreur, on affiche un message et on arrête tout
			}

		}
		
}


?>



<!DOCTYPE html>
<html lang="fr">

<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Jocelyne Gotschaux, artiste peintre, peinture, lyon, communay" />
  	<meta name="robots" content="index, follow" />
  	<meta name="author" content="Yannis Solémalé, Loick Bouchaut" />
  	<meta name="description" content="Jocelyne Gotschaux - artiste peintre, Lyon | Communay, 06 83 14 03 23" />

    <title>Contact</title>

    <?php include_once('include/head.php');?>

   <!-- Appels API Google Map -->
<!--   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>
  <script src="http://maps.google.com/maps-api-v3/api/js/24/0/intl/fr_ALL/common.js" charset="UTF-8" type="text/javascript"></script>
  <script src="http://maps.google.com/maps-api-v3/api/js/24/0/intl/fr_ALL/util.js" charset="UTF-8" type="text/javascript"></script>
  <script src="http://maps.google.com/maps-api-v3/api/js/24/0/intl/fr_ALL/geocoder.js" charset="UTF-8" type="text/javascript"></script>
  <script src="http://maps.google.com/maps-api-v3/api/js/24/0/intl/fr_ALL/map.js" charset="UTF-8" type="text/javascript"></script>
  <script src="http://maps.google.com/maps-api-v3/api/js/24/0/intl/fr_ALL/infowindow.js" charset="UTF-8" type="text/javascript"></script>
  <script src="http://maps.google.com/maps-api-v3/api/js/24/0/intl/fr_ALL/stats.js" charset="UTF-8" type="text/javascript"></script>
  <script src="http://maps.google.com/maps-api-v3/api/js/24/0/intl/fr_ALL/onion.js" charset="UTF-8" type="text/javascript"></script>
  <script src="http://maps.google.com/maps-api-v3/api/js/24/0/intl/fr_ALL/controls.js" charset="UTF-8" type="text/javascript"></script>
  <script src="http://maps.google.com/maps-api-v3/api/js/24/0/intl/fr_ALL/marker.js" charset="UTF-8" type="text/javascript"></script> -->
	

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

	<?php include_once('include/header.php'); ?>

    <div class="container">

    	<!-- SECTION MAP -->
        <div class="row">
            <div class="box">
			
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Contact
                        <strong>Jocelyne Gotschaux</strong>
                    </h2>
                    <hr>
                </div>
				
				
                <div class="col-sm-8 col-md-8 col-lg-8">
                    <!-- Embedded Google Map using an iframe - to select your location find it on Google maps and paste the link as the iframe src. If you want to use the Google Maps API instead then have at it! -->
                     <iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?hl=en&amp;ie=UTF8&amp;ll=45.6110602,4.8346922&amp;spn=56.506174,79.013672&amp;t=m&amp;z=18&amp;output=embed"></iframe>
					
					<div id="map" style="height: 100%; position: relative; background-color: rgb(229, 227, 223); overflow: hidden;"></div>
                </div>
				
				
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <p>Téléphone:
                        <strong>06 83 14 03 23</strong>
                    </p>
                    <p>Email:
                        <strong><a href="mailto:asscouleurs@yahoo.fr">asscouleurs@yahoo.fr</a></strong>
                    </p>
                    <p>Adresse:
                        <strong>1 Allée du Télégraphe
                            <br>69360 Communay</strong>
                    </p>
                </div>
				
				
                <div class="clearfix"></div>
            </div>
        </div>

		
		
		
		
		
		<!-- SECTION CONTACT -->
        <div class="row">
            <div class="box">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">
                        <strong>Me contacter</strong>
                    </h2>
                    <hr>
                    <p>Pour toute demande d'informations ou prise de contact n'hésitez pas à me contacter en pensant bien à laisser votre numéro de téléphone et votre adresse mail!</p>
                    
					
					
					<form method="post" id="contact" action="" role="form">
                        <div class="row">
                            <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                <label>Nom</label>
                                <input type="text" name="nom" class="form-control">
                            </div>
                            <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control">
                            </div>
                            <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                <label>Téléphone</label>
                                <input type="text" name="tel" class="form-control">
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                <label>Message</label>
                                <textarea class="form-control" rows="6" name="message"></textarea>
                            </div>
                            <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                <input type="hidden" name="contact" value="contact">
                                <button type="submit" class="btn btn-default">Envoyer le message</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
		
		
		
		<div class="row">
            <div class="box">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Livre d'or
                    </h2>
                    <hr>
                    <p>Si mon travail vous a plu, laissez moi un message pour le livre d'or, c'est toujours apprécié.</p>
                    
					
					
					<form method="post" id="livreor" action="" role="form">
                        <div class="row">
                            <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                <label>Nom</label>
                                <input type="text" name="nom" class="form-control">
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                <label>Message</label>
                                <textarea class="form-control" rows="6" name="message"></textarea>
                            </div>
                            <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                <input type="hidden" name="livreor" value="livreor">
                                <button type="submit" class="btn btn-default">Envoyer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    <?php //affichage messages livre d'or
	$req = $bdd->prepare('SELECT id, pseudo, message, date FROM peintre_livreor');
	$req->execute();

	$row = $req->fetchAll();

	if ($row!= NULL)
	echo "<div class='box'>";

	foreach($row as $livreor) { 
	
	?>
		<div class='row' style='min-height: 75px;'>
			<div class='col-sm-12 col-md-12 col-lg-12' style='text-align: center;'>		
				<?php echo $livreor['date'] .'  -  '. $livreor['pseudo'] .' a écrit : '. $livreor['message'];?>
			</div> 
		</div>
		<br>
	<?php } 
	if ($row != NULL)
	echo "</div>";
	?>

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer>
	<?php include_once('include/footer.php');?>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/grayscale.js"></script>
	
	<script type="text/javascript">


				// delay between geocode requests - at the time of writing, 100 miliseconds seems to work well
					var delay = 100;


					  // ====== Create map objects ======
					  var infowindow = new google.maps.InfoWindow();
					  var latlng = new google.maps.LatLng(45.7673621,4.8482694)
					  var mapOptions = 
					  {
						zoom: 8,
						center: latlng,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					  }
					  var geo = new google.maps.Geocoder(); 
					  var map = new google.maps.Map(document.getElementById("map"), mapOptions);
					  var bounds = new google.maps.LatLngBounds();

					  
					  
					  
					  
					  // ====== Geocoding ======
					  function getAddress(search, next) {
						geo.geocode({address:search}, function (results,status)
						  { 
							// If that was successful
							if (status == google.maps.GeocoderStatus.OK) {
							  // Lets assume that the first marker is the one we want
							  var p = results[0].geometry.location;
							  var lat=p.lat();
							  var lng=p.lng();
							  // Output the data
								//var msg = 'address="' + search + '" lat=' +lat+ ' lng=' +lng+ '(delay='+delay+'ms)<br>';
								//document.getElementById("messages").innerHTML += msg;
							  // Create a marker
							  createMarker(search,lat,lng);
							}
							// ====== Decode the error status ======
							else {
							  // === if we were sending the requests to fast, try this one again and increase the delay
							  if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
								nextAddress--;
								delay++;
							  } else {
								var reason="Code "+status;
								//var msg = 'address="' + search + '" error=' +reason+ '(delay='+delay+'ms)<br>';
								//document.getElementById("messages").innerHTML += msg;
							  }   
							}
							next();
						  }
						);
					  }

					 // ======= Function to create a marker
					 function createMarker(add,lat,lng) {
					   var contentString = add;
					   var marker = new google.maps.Marker({
						 position: new google.maps.LatLng(lat,lng),
						 map: map,
						 zIndex: Math.round(latlng.lat()*-100000)<<5
					   });

					  google.maps.event.addListener(marker, 'click', function() {
						 infowindow.setContent(contentString); 
						 infowindow.open(map,marker);
					   });

					   bounds.extend(marker.position);

					 }

					  // ======= An array of locations that we want to Geocode ========
					  var addresses = [
					  
						'1 Allée du Télégraphe 69360 Communay',
						'Lyon',

					  ];
					  
					  
					  

					  // ======= Global variable to remind us what to do next
					  var nextAddress = 0;

					  // ======= Function to call the next Geocode operation when the reply comes back

					  function theNext() {
						if (nextAddress < addresses.length) {
						  setTimeout('getAddress("'+addresses[nextAddress]+'",theNext)', delay);
						  nextAddress++;
						} else {
						  // We're done. Show map bounds
						  map.fitBounds(bounds);
						}
					  }

					  // ======= Call that function for the first time =======
					  theNext();

					// This Javascript is based on code provided by the
					// Community Church Javascript Team
					// http://www.bisphamchurch.org.uk/   
					// http://econym.org.uk/gmap/		
				  
				</script>
    </footer>

</body>

</html>