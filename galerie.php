<!DOCTYPE html>
<html lang="fr">

<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Jocelyne Gotschaux, artiste peintre, peinture, lyon, communay" />
  	<meta name="robots" content="index, follow" />
  	<meta name="author" content="Yannis Solémalé, Loick Bouchaut" />
  	<meta name="description" content="Jocelyne Gotschaux - artiste peintre, Lyon | Communay, 06 83 14 03 23" />

    <title>Galerie</title>

    <?php include_once('include/head.php');?>

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

	<?php include_once('include/header.php');?>

 <!-- Intro Header -->
    <!-- <div class="container"> -->
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12">

				<div id="carousel-example-generic" class="carousel slide cp" data-ride="carousel">
					  <!-- Indicators -->
					  <ol class="carousel-indicators">
						<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
						<li data-target="#carousel-example-generic" data-slide-to="1"></li>
						<li data-target="#carousel-example-generic" data-slide-to="2"></li>
						<li data-target="#carousel-example-generic" data-slide-to="3"></li>
					  </ol>

					  <!-- Wrapper for slides -->
					  <div class="carousel-inner cip" role="listbox">

						<div class="fhp item active">
						  
							<!-- <header class="intro" style="background: url(img/photo/carousel/849486d5d33ed462ff4167606a11968e5ae3ab0aa0ac24156.jpg) no-repeat bottom center scroll; background-size: 60% 100% ; "> -->
							<header class="intro">
								<div class="intro-body">
									<div class="container">
										<div class="row">
					      					<img style="max-height: 600px; max-width: 100%; display: block; margin-left: auto; margin-right: auto" src="img/photo/carousel/849486d5d33ed462ff4167606a11968e5ae3ab0aa0ac24156.jpg">
										</div>
									</div>
								</div>
							</header>
						  
						</div>
						
						<div class="fhp item">
						  
							<!-- <header class="intro" style="background: url(img/photo/carousel/7617801ac44ce5fd541cf02197af26b4eef0bc7f8bcb25767.jpg) no-repeat bottom center scroll; background-size: 100% 100%; "> -->
							<header class="intro">
								<div class="intro-body">
									<div class="container">
										<div class="row">
					      					<img style="max-height: 600px; max-width: 100%; display: block; margin-left: auto; margin-right: auto" src="img/photo/carousel/7617801ac44ce5fd541cf02197af26b4eef0bc7f8bcb25767.jpg">
										</div>
									</div>
								</div>
							</header>
						  
						</div>
						
						
						<div class="fhp item">
						  
							<!-- <header class="intro" style="background: url(img/photo/carousel/31428dd95a7c1c7706f8becd8a999104aca0c6fde691b22754.jpg) no-repeat bottom center scroll; background-size: 40% 100%; "> -->
							<header class="intro">
								<div class="intro-body">
									<div class="container">
										<div class="row">
					      					<img style="max-height: 600px; max-width: 100%; display: block; margin-left: auto; margin-right: auto" src="img/photo/carousel/31428dd95a7c1c7706f8becd8a999104aca0c6fde691b22754.jpg">
										</div>
									</div>
								</div>
							</header>
						  
						</div>
						
						
						<div class="fhp item">
						  
							<!-- <header class="intro" style="background: url(img/photo/carousel/16018aa3618930b2392d0bbfe803c1b264f7a0777eeb75216.jpg) no-repeat bottom center scroll; background-size: 100% 100%; "> -->
							<header class="intro">
								<div class="intro-body">
									<div class="container">
										<div class="row">
					      					<img style="max-height: 600px; max-width: 100%; display: block; margin-left: auto; margin-right: auto" src="img/photo/carousel/16018aa3618930b2392d0bbfe803c1b264f7a0777eeb75216.jpg">
										</div>
									</div>
								</div>
							</header>
						  
						</div>


					  </div>

					</div>
				</div>
			</div>
		<!-- </div> -->

		<script type="javascript">
		$('.carousel').carousel({
		  interval: 5000
		})
		</script>

		<br>

	
	
    <!-- Main Section -->
    <div id="main">
		<section id="main" class="content-section text-center">
			<div class="main-section-test">

			 	<div class="row">
	            	<div class="col-sm-2 col-md-2 col-lg-2">
					
						<?php
						


						include_once('include/connexion.php');
					

						global $bdd;

						$req = $bdd->prepare('SELECT id, nom FROM peintre_categories WHERE isvisible = 1 ');
						$req->execute();

						$data = $req->fetchAll();
						
						
						
						
						
						
						
						foreach($data as $cat)
						{
							
						?>	
							
							
					
	            		<h3><a href="galerie.php?id=<?php echo $cat['id'];?>"><?php echo $cat['nom']; ?></a></h3>

						
						<?php
						
						}
						
						?>
			 		</div>



					<?php
						
						if (isset($_GET['id'])) 
							{$categorieid = $_GET['id'];}
						else{$categorieid = 2;};

						$req = $bdd->prepare('SELECT id, titre, url FROM peintre_tableaux WHERE isvisible = 1 AND categorieid = :categorieid');
						$req->bindParam(':categorieid', $categorieid);
						$req->execute();

						$data = $req->fetchAll();
					?>
	
					<div class="col-sm-10 col-md-10 col-lg-10">	
						<!-- #region Jssor Slider Begin -->
						<!-- Generator: Jssor Slider Maker -->
						<!-- Source: http://www.jssor.com -->
						<!-- This code works with jquery library. -->
						<script asyc src="jssorr/js/jquery-1.11.3.min.js" type="text/javascript"></script>
						<script asyc src="jssorr/js/jssor.slider-21.1.5.mini.js" type="text/javascript"></script>
						<script type="text/javascript">
							jQuery(document).ready(function ($) {
								
								var jssor_1_SlideshowTransitions = [
								  {$Duration:1200,x:0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,x:-0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,y:0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,y:-0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,y:-0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,y:0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,x:0.3,$Cols:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,x:0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,y:0.3,$Rows:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,y:0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,y:0.3,$Cols:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,y:-0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,x:0.3,$Rows:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,x:-0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$SlideOut:true,$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,$Delay:20,$Clip:3,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,$Delay:20,$Clip:3,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,$Delay:20,$Clip:12,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
								  {$Duration:1200,$Delay:20,$Clip:12,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
								];
								
								var jssor_1_options = {
								  $AutoPlay: false,
								  $FillMode: 1,
								  $SlideshowOptions: {
									$Class: $JssorSlideshowRunner$,
									$Transitions: jssor_1_SlideshowTransitions,
									$TransitionsOrder: 1
								  },
								  $ArrowNavigatorOptions: {
									$Class: $JssorArrowNavigator$
								  },
								  $ThumbnailNavigatorOptions: {
									$Class: $JssorThumbnailNavigator$,
									$Cols: 10,
									$SpacingX: 8,
									$SpacingY: 8,
									$Align: 360
								  }
								};
								
								var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
								
								//responsive code begin
								//you can remove responsive code if you don't want the slider scales while window resizing
								function ScaleSlider() {
									var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
									if (refSize) {
										refSize = Math.min(refSize, 1200);
										jssor_1_slider.$ScaleWidth(refSize);
									}
									else {
										window.setTimeout(ScaleSlider, 30);
									}
								}
								ScaleSlider();
								$(window).bind("load", ScaleSlider);
								$(window).bind("resize", ScaleSlider);
								$(window).bind("orientationchange", ScaleSlider);
								//responsive code end
							});
							
						</script>
						<style>
							/* jssor slider arrow navigator skin 05 css */
							/*
							.jssora05l                  (normal)
							.jssora05r                  (normal)
							.jssora05l:hover            (normal mouseover)
							.jssora05r:hover            (normal mouseover)
							.jssora05l.jssora05ldn      (mousedown)
							.jssora05r.jssora05rdn      (mousedown)
							*/
							.jssora05l, .jssora05r {
								display: block;
								position: absolute;
								/* size of arrow element */
								width: 40px;
								height: 40px;
								cursor: pointer;
								background: url('img/a17.png') no-repeat;
								overflow: hidden;
							}
							.jssora05l { background-position: -10px -40px; }
							.jssora05r { background-position: -70px -40px; }
							.jssora05l:hover { background-position: -130px -40px; }
							.jssora05r:hover { background-position: -190px -40px; }
							.jssora05l.jssora05ldn { background-position: -250px -40px; }
							.jssora05r.jssora05rdn { background-position: -310px -40px; }
							
							/* jssor slider thumbnail navigator skin 01 css */
							/*
							.jssort01 .p            (normal)
							.jssort01 .p:hover      (normal mouseover)
							.jssort01 .p.pav        (active)
							.jssort01 .p.pdn        (mousedown)
							*/
							.jssort01 .p {
								position: absolute;
								top: 0;
								left: 0;
								width: 72px;
								height: 72px;
							}
							
							.jssort01 .t {
								position: absolute;
								top: 0;
								left: 0;
								width: 100%;
								height: 100%;
								border: none;
							}
							
							.jssort01 .w {
								position: absolute;
								top: 0px;
								left: 0px;
								width: 100%;
								height: 100%;
							}
							
							.jssort01 .c {
								position: absolute;
								top: 0px;
								left: 0px;
								width: 68px;
								height: 68px;
								border: #000 2px solid;
								box-sizing: content-box;
								background: url('jssorr/img/t01.png') -800px -800px no-repeat;
								_background: none;
							}
							
							.jssort01 .pav .c {
								top: 2px;
								_top: 0px;
								left: 2px;
								_left: 0px;
								width: 68px;
								height: 68px;
								border: #000 0px solid;
								_border: #fff 2px solid;
								background-position: 50% 50%;
							}
							
							.jssort01 .p:hover .c {
								top: 0px;
								left: 0px;
								width: 70px;
								height: 70px;
								border: #fff 1px solid;
								background-position: 50% 50%;
							}
							
							.jssort01 .p.pdn .c {
								background-position: 50% 50%;
								width: 68px;
								height: 68px;
								border: #000 2px solid;
							}
							
							* html .jssort01 .c, * html .jssort01 .pdn .c, * html .jssort01 .pav .c {
								/* ie quirks mode adjust */
								width /**/: 72px;
								height /**/: 72px;
							}
							
						</style>
						<!-- <div class="row"> -->
							<div class="col-sm-10 col-md-10 col-lg-10">

								<center>Images redimensionnées. Cliquez pour voir en taille réelle</center>
								<div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1200px; height: 794px; overflow: hidden; visibility: hidden; background-color: #000000;">
									
									<!-- Loading Screen -->
									<div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
										<div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
										<div style="position:absolute;display:block;background:url('jssorr/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
									</div>
									<div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1200px; height: 694px; overflow: hidden;">

										
										<?php
										foreach($data as $k=>$tab)
										{
										?>		
	
											<div data-p="144.50" style="display: none;">
												<a href="<?php echo $tab['url']; ?>">
													<img data-u="image" src="<?php echo 'img/tableaux/galerie/'.substr($tab['url'],14); ?>" style="max-height:594px"/>
													
													<h3 style="position: absolute; top: 90%; left: 0%; right: 0%; font-family: cursive; color: white;"><?php echo $tab['titre']; ?></h3>

												</a>


											</div>

		
										<?php
										
										}
										
										?>
											
										
									</div>


									<!-- Thumbnail Navigator -->
									<div data-u="thumbnavigator" class="jssort01" style="position:absolute;left:0px;bottom:0px;width:800px;height:100px;padding-top:100px;" data-autocenter="1">
										<!-- Thumbnail Item Skin Begin -->
										<div data-u="slides" style="cursor: default;">
											<div data-u="prototype" class="p">
												<div class="w">
													<div data-u="thumbnailtemplate" class="t"></div>
												</div>
												<div class="c"></div>
											</div>
										</div>

										<!-- Thumbnail Item Skin End -->
									</div>
									<!-- Arrow Navigator -->
									<span data-u="arrowleft" class="jssora05l" style="top:158px;left:8px;width:40px;height:40px;"></span>
									<span data-u="arrowright" class="jssora05r" style="top:158px;right:8px;width:40px;height:40px;"></span>
								</div>

							</div>

						</div>
						<!-- #endregion Jssor Slider End -->
						
						
					</div>	<!-- #endregion col 12 -->
	
	

    <!-- Footer -->
    <footer>
	<?php include_once('include/footer.php');?>
    </footer>

    <!-- jQuery -->
    <!-- <script src="js/jquery.js"></script> -->

    <!-- Plugin JavaScript -->
    <!-- <script src="js/jquery.easing.min.js"></script> -->

    <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
    <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script> -->

    <!-- Custom Theme JavaScript -->
    <!-- <script src="js/grayscale.js"></script> -->

</body>

</html>

    <script type="text/javascript">
    (function() {

		var initPhotoSwipeFromDOM = function(gallerySelector) {

			var parseThumbnailElements = function(el) {
			    var thumbElements = el.childNodes,
			        numNodes = thumbElements.length,
			        items = [],
			        el,
			        childElements,
			        thumbnailEl,
			        size,
			        item;

			    for(var i = 0; i < numNodes; i++) {
			        el = thumbElements[i];

			        // include only element nodes 
			        if(el.nodeType !== 1) {
			          continue;
			        }

			        childElements = el.children;

			        size = el.getAttribute('data-size').split('x');

			        // create slide object
			        item = {
						src: el.getAttribute('href'),
						w: parseInt(size[0], 10),
						h: parseInt(size[1], 10),
						author: el.getAttribute('data-author')
			        };

			        item.el = el; // save link to element for getThumbBoundsFn

			        if(childElements.length > 0) {
			          item.msrc = childElements[0].getAttribute('src'); // thumbnail url
			          if(childElements.length > 1) {
			              item.title = childElements[1].innerHTML; // caption (contents of figure)
			          }
			        }


					var mediumSrc = el.getAttribute('data-med');
		          	if(mediumSrc) {
		            	size = el.getAttribute('data-med-size').split('x');
		            	// "medium-sized" image
		            	item.m = {
		              		src: mediumSrc,
		              		w: parseInt(size[0], 10),
		              		h: parseInt(size[1], 10)
		            	};
		          	}
		          	// original image
		          	item.o = {
		          		src: item.src,
		          		w: item.w,
		          		h: item.h
		          	};

			        items.push(item);
			    }

			    return items;
			};

			// find nearest parent element
			var closest = function closest(el, fn) {
			    return el && ( fn(el) ? el : closest(el.parentNode, fn) );
			};

			var onThumbnailsClick = function(e) {
			    e = e || window.event;
			    e.preventDefault ? e.preventDefault() : e.returnValue = false;

			    var eTarget = e.target || e.srcElement;

			    var clickedListItem = closest(eTarget, function(el) {
			        return el.tagName === 'A';
			    });

			    if(!clickedListItem) {
			        return;
			    }

			    var clickedGallery = clickedListItem.parentNode;

			    var childNodes = clickedListItem.parentNode.childNodes,
			        numChildNodes = childNodes.length,
			        nodeIndex = 0,
			        index;

			    for (var i = 0; i < numChildNodes; i++) {
			        if(childNodes[i].nodeType !== 1) { 
			            continue; 
			        }

			        if(childNodes[i] === clickedListItem) {
			            index = nodeIndex;
			            break;
			        }
			        nodeIndex++;
			    }

			    if(index >= 0) {
			        openPhotoSwipe( index, clickedGallery );
			    }
			    return false;
			};

			var photoswipeParseHash = function() {
				var hash = window.location.hash.substring(1),
			    params = {};

			    if(hash.length < 5) { // pid=1
			        return params;
			    }

			    var vars = hash.split('&');
			    for (var i = 0; i < vars.length; i++) {
			        if(!vars[i]) {
			            continue;
			        }
			        var pair = vars[i].split('=');  
			        if(pair.length < 2) {
			            continue;
			        }           
			        params[pair[0]] = pair[1];
			    }

			    if(params.gid) {
			    	params.gid = parseInt(params.gid, 10);
			    }

			    return params;
			};

			var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
			    var pswpElement = document.querySelectorAll('.pswp')[0],
			        gallery,
			        options,
			        items;

				items = parseThumbnailElements(galleryElement);

			    // define options (if needed)
			    options = {

			        galleryUID: galleryElement.getAttribute('data-pswp-uid'),

			        getThumbBoundsFn: function(index) {
			            // See Options->getThumbBoundsFn section of docs for more info
			            var thumbnail = items[index].el.children[0],
			                pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
			                rect = thumbnail.getBoundingClientRect(); 

			            return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
			        },

			        addCaptionHTMLFn: function(item, captionEl, isFake) {
						if(!item.title) {
							captionEl.children[0].innerText = '';
							return false;
						}
						captionEl.children[0].innerHTML = item.title +  '<br/><small>Photo: ' + item.author + '</small>';
						return true;
			        }
					
			    };


			    if(fromURL) {
			    	if(options.galleryPIDs) {
			    		// parse real index when custom PIDs are used 
			    		// http://photoswipe.com/documentation/faq.html#custom-pid-in-url
			    		for(var j = 0; j < items.length; j++) {
			    			if(items[j].pid == index) {
			    				options.index = j;
			    				break;
			    			}
			    		}
				    } else {
				    	options.index = parseInt(index, 10) - 1;
				    }
			    } else {
			    	options.index = parseInt(index, 10);
			    }

			    // exit if index not found
			    if( isNaN(options.index) ) {
			    	return;
			    }



				var radios = document.getElementsByName('gallery-style');
				for (var i = 0, length = radios.length; i < length; i++) {
				    if (radios[i].checked) {
				        if(radios[i].id == 'radio-all-controls') {

				        } else if(radios[i].id == 'radio-minimal-black') {
				        	options.mainClass = 'pswp--minimal--dark';
					        options.barsSize = {top:0,bottom:0};
							options.captionEl = false;
							options.fullscreenEl = false;
							options.shareEl = false;
							options.bgOpacity = 0.85;
							options.tapToClose = true;
							options.tapToToggleControls = false;
				        }
				        break;
				    }
				}

			    if(disableAnimation) {
			        options.showAnimationDuration = 0;
			    }

			    // Pass data to PhotoSwipe and initialize it
			    gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);

			    // see: http://photoswipe.com/documentation/responsive-images.html
				var realViewportWidth,
				    useLargeImages = false,
				    firstResize = true,
				    imageSrcWillChange;

				gallery.listen('beforeResize', function() {

					var dpiRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;
					dpiRatio = Math.min(dpiRatio, 2.5);
				    realViewportWidth = gallery.viewportSize.x * dpiRatio;


				    if(realViewportWidth >= 1200 || (!gallery.likelyTouchDevice && realViewportWidth > 800) || screen.width > 1200 ) {
				    	if(!useLargeImages) {
				    		useLargeImages = true;
				        	imageSrcWillChange = true;
				    	}
				        
				    } else {
				    	if(useLargeImages) {
				    		useLargeImages = false;
				        	imageSrcWillChange = true;
				    	}
				    }

				    if(imageSrcWillChange && !firstResize) {
				        gallery.invalidateCurrItems();
				    }

				    if(firstResize) {
				        firstResize = false;
				    }

				    imageSrcWillChange = false;

				});

				gallery.listen('gettingData', function(index, item) {
				    if( useLargeImages ) {
				        item.src = item.o.src;
				        item.w = item.o.w;
				        item.h = item.o.h;
				    } else {
				        item.src = item.m.src;
				        item.w = item.m.w;
				        item.h = item.m.h;
				    }
				});

			    gallery.init();
			};

			// select all gallery elements
			var galleryElements = document.querySelectorAll( gallerySelector );
			for(var i = 0, l = galleryElements.length; i < l; i++) {
				galleryElements[i].setAttribute('data-pswp-uid', i+1);
				galleryElements[i].onclick = onThumbnailsClick;
			}

			// Parse URL and open gallery if it contains #&pid=3&gid=1
			var hashData = photoswipeParseHash();
			if(hashData.pid && hashData.gid) {
				openPhotoSwipe( hashData.pid,  galleryElements[ hashData.gid - 1 ], true, true );
			}
		};

		initPhotoSwipeFromDOM('.demo-gallery');

	})();

	</script>
	
	
	
	
	
	
	
	
	
	
	