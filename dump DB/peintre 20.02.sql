-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Sam 20 Février 2016 à 18:41
-- Version du serveur :  5.7.9
-- Version de PHP :  5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `peintre`
--

-- --------------------------------------------------------

--
-- Structure de la table `peintre_categories`
--

DROP TABLE IF EXISTS `peintre_categories`;
CREATE TABLE IF NOT EXISTS `peintre_categories` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `isvisible` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `peintre_expositions`
--

DROP TABLE IF EXISTS `peintre_expositions`;
CREATE TABLE IF NOT EXISTS `peintre_expositions` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `isvisible` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `peintre_livreor`
--

DROP TABLE IF EXISTS `peintre_livreor`;
CREATE TABLE IF NOT EXISTS `peintre_livreor` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `peintre_tableaux`
--

DROP TABLE IF EXISTS `peintre_tableaux`;
CREATE TABLE IF NOT EXISTS `peintre_tableaux` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `categorie_id` int(15) NOT NULL,
  `exposition_id` int(15) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `isvisble` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `FK_categorie_id` (`categorie_id`),
  UNIQUE KEY `FK_exposition_id` (`exposition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `peintre_tableaux`
--
ALTER TABLE `peintre_tableaux`
  ADD CONSTRAINT `CONSTRAINT_categorie` FOREIGN KEY (`categorie_id`) REFERENCES `peintre_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `CONSTRAINT_exposition` FOREIGN KEY (`exposition_id`) REFERENCES `peintre_expositions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
