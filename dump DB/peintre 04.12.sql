-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Dim 04 Décembre 2016 à 18:36
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `peintre`
--

-- --------------------------------------------------------

--
-- Structure de la table `peintre_categories`
--

CREATE TABLE IF NOT EXISTS `peintre_categories` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `isvisible` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `peintre_categories`
--

INSERT INTO `peintre_categories` (`id`, `nom`, `isvisible`) VALUES
(1, 'Etudes de gris', 1),
(2, 'Paysages', 1),
(3, 'Monuments', 1),
(4, 'Musiciens', 1);

-- --------------------------------------------------------

--
-- Structure de la table `peintre_expositions`
--

CREATE TABLE IF NOT EXISTS `peintre_expositions` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `isvisible` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `peintre_expositions`
--

INSERT INTO `peintre_expositions` (`id`, `nom`, `isvisible`) VALUES
(1, 'Exposition sainte croix en jarez juillet 2015', 1),
(2, 'Exposition salon Regain en septembre 2016', 1);

-- --------------------------------------------------------

--
-- Structure de la table `peintre_livreor`
--

CREATE TABLE IF NOT EXISTS `peintre_livreor` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) DEFAULT NULL,
  `message` text,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `peintre_photo`
--

CREATE TABLE IF NOT EXISTS `peintre_photo` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `expositionid` int(2) DEFAULT NULL,
  `titre` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `isvisible` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `expositionid` (`expositionid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `peintre_photo`
--

INSERT INTO `peintre_photo` (`id`, `expositionid`, `titre`, `url`, `isvisible`) VALUES
(4, 1, 'Photo 1', './img/photo/21894d97c7c02e478b755cc790c3725453320532bdd1b18036.jpg', 1),
(5, 1, 'Photo 2', './img/photo/154275e77b23a0a786934e73c5f13c77ab2d7c23225667161.jpg', 1),
(6, 1, 'Photo 3', './img/photo/30526df6e6e14b32ff3aa9caf580a24f99dffbb32407a19180.jpg', 1),
(7, 1, 'Photo 4', './img/photo/290188fffff5370b8228793590ab153f5c5698bf2134b14440.jpg', 1),
(8, 1, 'Photo 5', './img/photo/32437b0c1aa285cdff14b3c77313e8af5d3e92c442f7c28859.jpg', 1),
(9, 2, 'Peindre dans la rue Alpes d''huez', './img/photo/298947089c4853592ca4b0fe50ebfb1fe0670c36a86746068.jpg', 1);

-- --------------------------------------------------------

--
-- Structure de la table `peintre_tableaux`
--

CREATE TABLE IF NOT EXISTS `peintre_tableaux` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `categorieid` int(2) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `isvisible` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `categorieid` (`categorieid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

--
-- Contenu de la table `peintre_tableaux`
--

INSERT INTO `peintre_tableaux` (`id`, `categorieid`, `titre`, `url`, `isvisible`) VALUES
(25, 3, 'Abbatiale St JEAN FÃªte des LUMIERES 2', './img/tableaux/1931375918f7e2b223c836d58b64ca7c03ecc04ed7910471.jpg', 1),
(26, 2, 'CASCADES', './img/tableaux/176892aa9c99280c2fe0c0a5f1320534061123725660d30655.jpg', 1),
(27, 3, 'CathÃ©drale St MAURICE VIENNE', './img/tableaux/31428dd95a7c1c7706f8becd8a999104aca0c6fde691b22754.jpg', 1),
(28, 2, 'DESERT BLANC 1', './img/tableaux/242042a92a664bbdec3c602fc11bb6a1fad0a5df828395610.jpg', 1),
(29, 2, 'DESERT BLANC 2', './img/tableaux/257150c7793a22195ae2ba4345ff1ded2720d8f291d4030633.jpg', 1),
(30, 2, 'DESERT NOIR', './img/tableaux/2069998150a37475bfa4f81c16edacd20f356c576f32132337.jpg', 1),
(31, 2, 'FETE du MASQUAL', './img/tableaux/849486d5d33ed462ff4167606a11968e5ae3ab0aa0ac24156.jpg', 1),
(32, 2, 'Jardin du MUSEE sous la NEIGE', './img/tableaux/99108ee4c282ef5fc6666c24942d99f140f88df6725329476.jpg', 1),
(33, 2, 'La grotte CHAUVET 1', './img/tableaux/25957bfe896711ae995a98890892d89269db6afc86c6e28395.jpg', 1),
(34, 2, 'La grotte CHAUVET 2', './img/tableaux/1191746b12d4e2f7935e6e30865bfbeceaed35575251583.jpg', 1),
(35, 2, 'LAC en AUTOMMNE', './img/tableaux/756892051bda7eb9f844660b2f4b9a3d0e3eaa4aa28822158.jpg', 1),
(36, 2, 'Le MONT-MAUDIT', './img/tableaux/10960f23fd02eb81b339fdb189176e28ea3150e1f5b110246.jpg', 1),
(37, 2, 'MONTAGNE BLANC sur NOIR', './img/tableaux/2014385b7bdc8c6426f3d183d2e2a73f32b5082b0bacd8773.jpg', 1),
(38, 2, 'MONTAGNE BLEUE', './img/tableaux/3783fd374a53a72540a4694059a31330255eb73728e423581.jpg', 1),
(40, 2, 'MONTAGNE dans les NUAGES', './img/tableaux/16018aa3618930b2392d0bbfe803c1b264f7a0777eeb75216.jpg', 1),
(41, 1, 'MusÃ©e CONFLUENCES', './img/tableaux/7617801ac44ce5fd541cf02197af26b4eef0bc7f8bcb25767.jpg', 1),
(42, 3, 'NOTRE-DAME de PARIS', './img/tableaux/74492e263d1cf1cc3bc72321a143679a1cb607d6472b4031.jpg', 1),
(43, 3, 'OPERA DÃªte des LUMIERES', './img/tableaux/279887e524b8e41065e5543d3bd54f97e54aa31b9fa8030194.jpg', 1),
(44, 4, 'ORCHESTRE2', './img/tableaux/57723017d9bc3c1891025fa5fd5c65403897c920dee13562.jpg', 1),
(45, 4, 'ORCHESTRE3', './img/tableaux/92833fd988c4f8401da7303ff5b6e3fafda5b6989f2e17401.jpg', 1),
(46, 2, 'Passerelle St GEORGES', './img/tableaux/26843d82af93ed5949502f44ba24405cc10631896ad7a22097.jpg', 1),
(47, 2, 'Place de BELLECOUR', './img/tableaux/325749bf4934daad0d9e9eeca88f213576add55cabae14844.jpg', 1),
(48, 1, 'Pluie sur NEW-YORK', './img/tableaux/78620001bf207ca0475a5efc4416efb2c22991db1aa011044.jpg', 1),
(49, 3, 'PUZZLE Notre-Dame de PARIS', './img/tableaux/1677710cbf0b23e1de92349031277d3445e6aed1bbb16227.jpg', 1),
(50, 4, 'QUATUOR', './img/tableaux/9909ba8f6d3542c804230bcab4001a95596dc26e892122715.jpg', 1),
(51, 3, 'Reflets des Vitraux', './img/tableaux/21904686028f27e74a1b054cb7645f12f40891ea9b50020110.jpg', 1),
(52, 1, 'Rue de NEW-YORK1', './img/tableaux/291447c8999e6f661fc7203a1580782a9ebc53f6591c414358.jpg', 1),
(53, 1, 'Rue de NEW-YORK2', './img/tableaux/262872548b3df29962496ea171ed91ec59fd9ed4eb6c431301.jpg', 1),
(54, 1, 'Rue de NEW-YORK3', './img/tableaux/178150e13e91b9a13c15c4f5167196378077e50b64a9130573.jpg', 1),
(55, 3, 'SAGRADA FAMILIA', './img/tableaux/17818dab96df4b3c1e1fd61de3710cf221fce8ad74a2c18088.jpg', 1),
(56, 2, 'San FRANCISCO', './img/tableaux/16290f2c2a47649324b7d6b6f1f6d8790ac802331da599200.jpg', 1),
(57, 1, 'TOITS de PARIS 1', './img/tableaux/301455c7e402ed5c7377904acd4e9ba2634c6e728ec1c31911.jpg', 1),
(58, 1, 'TOITS de PARIS 2', './img/tableaux/7529e5a933d099af1bf3053474ed6b2b897ce8ae46bf20623.jpg', 1),
(59, 3, 'Visite de l''ABBAYE', './img/tableaux/9556a0a5a8765e3688d9070599cb1edfdd8f1130c40328146.jpg', 1),
(60, 3, 'CathÃ©drale St JEAN', './img/tableaux/3868b3c1341cb5b4d5e179f528390b38b9fb88df47788442.jpg', 1);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `peintre_photo`
--
ALTER TABLE `peintre_photo`
  ADD CONSTRAINT `EXPO_FK` FOREIGN KEY (`expositionid`) REFERENCES `peintre_expositions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `peintre_tableaux`
--
ALTER TABLE `peintre_tableaux`
  ADD CONSTRAINT `CAT_FK` FOREIGN KEY (`categorieid`) REFERENCES `peintre_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
