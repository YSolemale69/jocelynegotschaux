-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Client :  peintre.mysql.db
-- Généré le :  Sam 30 Juillet 2016 à 20:58
-- Version du serveur :  5.5.49-0+deb7u1-log
-- Version de PHP :  5.4.45-0+deb7u4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `peintre`
--

-- --------------------------------------------------------

--
-- Structure de la table `peintre_categories`
--

CREATE TABLE IF NOT EXISTS `peintre_categories` (
  `id` int(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `isvisible` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `peintre_categories`
--

INSERT INTO `peintre_categories` (`id`, `nom`, `isvisible`) VALUES
(5, 'Etudes de gris', 1),
(6, 'Paysages', 1),
(7, 'Monuments', 1),
(8, 'Musiciens', 1),
(9, 'Anciens', 1);

-- --------------------------------------------------------

--
-- Structure de la table `peintre_expositions`
--

CREATE TABLE IF NOT EXISTS `peintre_expositions` (
  `id` int(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `isvisible` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `peintre_expositions`
--

INSERT INTO `peintre_expositions` (`id`, `nom`, `isvisible`) VALUES
(1, 'Expo 1', 1),
(2, 'Expo 2', 1);

-- --------------------------------------------------------

--
-- Structure de la table `peintre_galeries`
--

CREATE TABLE IF NOT EXISTS `peintre_galeries` (
  `id` int(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `isvisible` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `peintre_livreor`
--

CREATE TABLE IF NOT EXISTS `peintre_livreor` (
  `id` int(255) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `peintre_livreor`
--

INSERT INTO `peintre_livreor` (`id`, `pseudo`, `message`, `date`) VALUES
(1, 'test', 'test', '2016-03-11 09:40:34');

-- --------------------------------------------------------

--
-- Structure de la table `peintre_photo`
--

CREATE TABLE IF NOT EXISTS `peintre_photo` (
  `id` int(255) NOT NULL,
  `expositionid` int(10) DEFAULT NULL,
  `titre` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `isvisible` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `peintre_tableaux`
--

CREATE TABLE IF NOT EXISTS `peintre_tableaux` (
  `id` int(255) NOT NULL,
  `categorieid` int(10) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `isvisible` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `peintre_categories`
--
ALTER TABLE `peintre_categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `peintre_expositions`
--
ALTER TABLE `peintre_expositions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `peintre_galeries`
--
ALTER TABLE `peintre_galeries`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `peintre_livreor`
--
ALTER TABLE `peintre_livreor`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `peintre_photo`
--
ALTER TABLE `peintre_photo`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `peintre_tableaux`
--
ALTER TABLE `peintre_tableaux`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `peintre_categories`
--
ALTER TABLE `peintre_categories`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `peintre_expositions`
--
ALTER TABLE `peintre_expositions`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `peintre_galeries`
--
ALTER TABLE `peintre_galeries`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `peintre_livreor`
--
ALTER TABLE `peintre_livreor`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `peintre_photo`
--
ALTER TABLE `peintre_photo`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `peintre_tableaux`
--
ALTER TABLE `peintre_tableaux`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
