-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Dim 31 Juillet 2016 à 15:56
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `peintre`
--

-- --------------------------------------------------------

--
-- Structure de la table `peintre_categories`
--

CREATE TABLE IF NOT EXISTS `peintre_categories` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `isvisible` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `peintre_categories`
--

INSERT INTO `peintre_categories` (`id`, `nom`, `isvisible`) VALUES
(1, 'Etudes de gris', 1),
(2, 'Paysages', 1),
(3, 'Monuments', 1),
(4, 'Musiciens', 1),
(5, 'Anciens', 1);

-- --------------------------------------------------------

--
-- Structure de la table `peintre_expositions`
--

CREATE TABLE IF NOT EXISTS `peintre_expositions` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `isvisible` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `peintre_expositions`
--

INSERT INTO `peintre_expositions` (`id`, `nom`, `isvisible`) VALUES
(1, 'Expo 1', 1),
(2, 'Expo 2', 1);

-- --------------------------------------------------------

--
-- Structure de la table `peintre_livreor`
--

CREATE TABLE IF NOT EXISTS `peintre_livreor` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) DEFAULT NULL,
  `message` text,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `peintre_livreor`
--

INSERT INTO `peintre_livreor` (`id`, `pseudo`, `message`, `date`) VALUES
(1, 'test', 'test', '2016-03-11 09:40:34');

-- --------------------------------------------------------

--
-- Structure de la table `peintre_photo`
--

CREATE TABLE IF NOT EXISTS `peintre_photo` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `expositionid` int(2) DEFAULT NULL,
  `titre` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `isvisible` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `expositionid` (`expositionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `peintre_tableaux`
--

CREATE TABLE IF NOT EXISTS `peintre_tableaux` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `categorieid` int(2) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `isvisible` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `categorieid` (`categorieid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `peintre_tableaux`
--

INSERT INTO `peintre_tableaux` (`id`, `categorieid`, `titre`, `url`, `isvisible`) VALUES
(1, 1, 'sfsdf', '\\img\\tableaux\\IMG_0003.JPG', 1),
(2, 2, 'rthh', '\\img\\tableaux\\IMG_0003.JPG', 1),
(3, 3, 'ezere', '\\img\\tableaux\\IMG_0007.JPG', 1),
(4, 4, 'erg', '\\img\\tableaux\\IMG_0014.JPG', 1),
(5, 5, 'tyjty', '\\img\\tableaux\\IMG_0012.JPG', 1);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `peintre_photo`
--
ALTER TABLE `peintre_photo`
  ADD CONSTRAINT `EXPO_FK` FOREIGN KEY (`expositionid`) REFERENCES `peintre_expositions` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `peintre_tableaux`
--
ALTER TABLE `peintre_tableaux`
  ADD CONSTRAINT `CAT_FK` FOREIGN KEY (`categorieid`) REFERENCES `peintre_tableaux` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
